import React, { useReducer, useEffect } from 'react';
import styled from 'styled-components';

import produce from 'immer';

import Item from './item';
import { motion } from 'framer-motion';
import { useIsPortrait } from './hooks';

import victoriaCoren from './VC.png';

const WallDiv = styled(motion.div)`
  position: relative;
  height: 100vh;
  width: 100%;
`;

const CorenDiv = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100vh;
`;

const CorenImg = styled(motion.img)`
  width: 100%;
`;

const CorenMsg = styled(motion.div)`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  font-size: 4vh;
  padding: 1rem;
  text-align: center;
`;

const reducer = (state, { row, column }) => {
  if (row < state.solved) return state; // can't click already solved rows
  const numberOfClickedItems = state.items.filter(x => x.clicked).length;

  if (numberOfClickedItems < 4)
    return produce(state, draftState => {
      const item = draftState.items.find(
        item => item.row === row && item.column === column
      );

      item.clicked = !item.clicked;

      const clickedItems = draftState.items.filter(x => x.clicked);
      if (clickedItems.length < 4) return draftState; // because they might have unclicked instead

      if (clickedItems.every(x => x.group === clickedItems[0].group)) {
        clickedItems.forEach((item, index) => {
          const swap = { ...item };
          const homeItem = draftState.items.find(
            x => x.row === draftState.solved && x.column === index
          );
          item.row = draftState.solved;
          item.column = index;
          homeItem.row = swap.row;
          homeItem.column = swap.column;
        });
        if (draftState.solved === 2) draftState.solved = 4;
        else draftState.solved++;
      }

      // need to cancel all clicks regardless
      draftState.items.forEach(item => {
        item.clicked = false;
      });
    });
};

function shuffle(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

const wallToInitialState = ({ wall }) => {
  const output = [];
  const groups = wall.map(group => group.items);

  const positions = [];
  for (let row = 0; row <= 3; row++) {
    for (let column = 0; column <= 3; column++) {
      positions.push({ row, column });
    }
  }
  shuffle(positions);
  groups.forEach((group, index) => {
    for (const item of group) {
      output.push({
        text: item,
        clicked: false,
        ...positions.pop(),
        group: index + 1,
      });
    }
  });

  return { items: output, solved: 0 };
};

const Wall = ({ wall, onSolve }) => {
  const [state, dispatch] = useReducer(reducer, wallToInitialState(wall));

  useEffect(() => {
    if (state.solved === 4) onSolve();
    // eslint-disable-next-line
  }, [state.solved]);

  const isPortrait = useIsPortrait();
  if (isPortrait)
    return (
      <CorenDiv>
        <CorenMsg initial={{ opacity: 0 }} animate={{ opacity: 1 }}>
          Please turn your screen to be landscape!
        </CorenMsg>
        <CorenImg
          initial={{ transform: 'translateY(100%)' }}
          animate={{ transform: 'translateY(0%)' }}
          transition={{ damping: 500 }}
          src={victoriaCoren}
        />
      </CorenDiv>
    );

  return (
    <WallDiv animate={{ opacity: 1 }} initial={{ opacity: 0 }}>
      {state.items.map(({ row, column, clicked, text }, index) => (
        <Item
          index={index}
          key={index}
          solved={state.solved}
          row={row}
          column={column}
          clicked={clicked}
          onClick={(row, column) => dispatch({ row, column })}
          text={text}
        />
      ))}
    </WallDiv>
  );
};

export default Wall;
