import React from 'react';

import walls from './walls';
import Puzzle from './puzzle';

const App = () => {
  const { pathname } = window.location;
  const wall = walls[pathname.slice(1)];
  if (wall) return <Puzzle wall={wall} />;
  return (
    <div>
      I don't think we have the "{pathname.slice(1) || 'blank'}" wall, sorry!
    </div>
  );
};

export default App;
