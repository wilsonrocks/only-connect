import React from 'react';
import styled from 'styled-components';
import Group from './group';
import { motion } from 'framer-motion';

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const WellDoneContainer = styled(motion.div)`
  align-self: center;
`;

const Groups = ({
  wall: { wall },
  groupsSolved,
  solveGroup,
  acknowledgeSolution,
}) => {
  return (
    <Container>
      {wall.map(({ regex, items, explanation }, index) => (
        <Group
          key={index}
          regex={regex}
          items={items}
          solved={groupsSolved[index]}
          solveGroup={() => solveGroup(index + 1)}
          explanation={explanation}
        />
      ))}
      {groupsSolved.every(x => x) && (
        <WellDoneContainer
          animate={{ transform: 'scale(1)' }}
          initial={{ transform: 'scale(0)' }}
        >
          <strong>Well done!</strong>{' '}
          <button type="button" onClick={acknowledgeSolution}>
            Continue
          </button>
        </WellDoneContainer>
      )}
    </Container>
  );
};

export default Groups;
