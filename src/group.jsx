import React, { useState } from 'react';
import styled from 'styled-components';

const Row = styled.div`
  padding: 1rem;
  input {
    max-width: 100%;
  }
`;

const Group = ({ regex, items, solved, solveGroup, explanation }) => {
  const [text, setText] = useState('');
  if (solved)
    return (
      <Row>
        <strong>Correct!</strong> {explanation}
      </Row>
    );

  return (
    <Row>
      <form
        onSubmit={event => {
          event.preventDefault();
          if (text.match(regex)) solveGroup();
          else {
            alert('sadly not');
            setText('');
          }
        }}
      >
        <span>
          What links <strong>{items[0]}</strong>, <strong>{items[1]}</strong>,{' '}
          <strong>{items[2]}</strong> and <strong>{items[3]}</strong>?
        </span>
        <div>
          <input value={text} onChange={event => setText(event.target.value)} />
          <button type="submit  ">Is this right?</button>
        </div>
      </form>
    </Row>
  );
};

export default Group;
