import { useEffect, useState } from 'react';

export const useIsPortrait = () => {
  const isPortrait = window.innerHeight > window.innerWidth;
  const [output, setOutput] = useState(isPortrait);

  useEffect(() => {
    window.onresize = () => {
      setOutput(window.innerHeight > window.innerWidth);
    };
    return () => {
      window.onresize = undefined;
    };
  }, [isPortrait]);

  return output;
};
