import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';

const Container = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-size: 3rem;
  padding: 2rem;
`;

const Answer = styled(motion.pre)``;

const Code = ({ code }) => (
  <Container>
    <span>The secret code is</span>
    <Answer
      animate={{ transform: 'scale(1)', opacity: 1 }}
      initial={{ transform: 'scale(100)', opacity: 0 }}
      transition={{ delay: 1, damping: 20 }}
    >
      {code}
    </Answer>
  </Container>
);

export default Code;
