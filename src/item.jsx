import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';

const guides = ['20%', '140%', '260%', '380%'];

const ItemBox = styled(motion.button)`
  width: 20%;
  height: 20%;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 5px;
  font-size: 4vh;
  text-align: center;
  z-index: ${props => props.index};
  border: 0;
`;

const PALE_BLUE = '#aed7f6';
const DARK_BLUE = '#054772';
const GREEN = '#047a5a';
const PURPLE = '#562042';
const TEAL = '#046e7c';

const getBackgroundColour = (solved, row) =>
  [
    [PALE_BLUE, PALE_BLUE, PALE_BLUE, PALE_BLUE],
    [DARK_BLUE, PALE_BLUE, PALE_BLUE, PALE_BLUE],
    [DARK_BLUE, GREEN, PALE_BLUE, PALE_BLUE],
    [DARK_BLUE, GREEN, PURPLE, PALE_BLUE],
    [DARK_BLUE, GREEN, PURPLE, TEAL],
  ][solved][row];

const clickedColour = solved => [DARK_BLUE, GREEN, PURPLE, TEAL][solved];

const Item = ({
  row = 0,
  column = 0,
  onClick,
  clicked,
  solved,
  text,
  index,
}) => {
  return (
    <ItemBox
      tabIndex={1 + row * 4 + column}
      index={index}
      animate={{
        transform: `translate3d(${guides[column]},${guides[row]},0)`,
        backgroundColor: clicked
          ? clickedColour(solved)
          : getBackgroundColour(solved, row),
        color: clicked || row < solved ? '#FFFFFF' : '#000000',
        textShadow:
          clicked || row < solved
            ? '0 1px #000000'
            : `0 1px ${getBackgroundColour(solved, row)}`,
      }}
      transition={{ damping: 5000 }}
      initial={false}
      onClick={event => {
        onClick(row, column);
        event.target.blur();
      }}
    >
      {text}
    </ItemBox>
  );
};

export default Item;
