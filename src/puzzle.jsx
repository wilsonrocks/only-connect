import React, { useReducer } from 'react';
import Wall from './wall';
import Groups from './groups';
import produce from 'immer';
import './index.css';
import Code from './code';

const initialState = {
  wallSolved: false,
  displayingCompletedWall: false,
  groupsSolved: [false, false, false, false],
  solutionAcknowledged: false,
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'SOLVE_WALL':
      return { ...state, wallSolved: true, displayingCompletedWall: true };

    case 'FINISH_DISPLAYING_COMPLETED_WALL':
      return { ...state, displayingCompletedWall: false };

    case 'SOLVE_GROUP':
      return produce(state, draftState => {
        const { group } = action;
        draftState.groupsSolved[group - 1] = true;
      });

    case 'ACKNOWLEDGE_SOLUTION':
      return { ...state, solutionAcknowledged: true };

    default:
      return state;
  }
};

function Puzzle({ wall }) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const {
    wallSolved,
    groupsSolved,
    solutionAcknowledged,
    displayingCompletedWall,
  } = state;

  const onSolve = () => {
    dispatch({ type: 'SOLVE_WALL' });
    setTimeout(() => {
      dispatch({ type: 'FINISH_DISPLAYING_COMPLETED_WALL' });
    }, 1000);
  };

  if (wallSolved === false || displayingCompletedWall)
    return <Wall wall={wall} onSolve={onSolve} />;

  if (!solutionAcknowledged)
    return (
      <Groups
        groupsSolved={groupsSolved}
        wall={wall}
        solveGroup={group => dispatch({ type: 'SOLVE_GROUP', group })}
        acknowledgeSolution={() => dispatch({ type: 'ACKNOWLEDGE_SOLUTION' })}
      />
    );

  return <Code code={wall.code} />;
}

export default Puzzle;
