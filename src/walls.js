const walls = {
  shelley: {
    code: 'CHICKY HAIR',
    wall: [
      {
        regex: /berry|berries/i,
        items: ['Chuck', 'Logan', 'Rasp', 'Straw'],
        explanation: 'They are all types of berry',
      },
      {
        regex: /chuck/i,
        items: ['D', 'Ragan', 'Berry', 'Taylor'],
        explanation: 'They are all famous Chucks',
      },
      {
        regex: /bass/i,
        items: ['Soon', 'Clef', 'Line', 'Guitar'],
        explanation: "They can all be preceded by 'bass'",
      },
      {
        regex: /hugh jackman/i,
        items: [
          'The Greatest Showman',
          'Les Miserables',
          'Real Steel',
          'Flushed Away',
        ],
        explanation: 'They are all films starring Hugh Jackman',
      },
    ],
  },
  bryony: {
    code: 'IF YOU LIKE PINA COLADAS',
    wall: [
      {
        regex: /rice/i,
        items: ['White', 'Brown', 'Lemon', 'Sticky'],
        explanation: 'They are all types of rice',
      },
      {
        regex: /tea/i,
        items: ['Jasmine', 'Green', 'Cream', 'High'],
        explanation: 'They are all types of tea',
      },
      {
        regex: /black/i,
        items: ['Flag', 'Berry', 'Bag', 'Box'],
        explanation: "They can all be preceded by 'black'",
      },
      {
        regex: /bin|bins/i,
        items: ['Salt', 'Bread', 'Dust', 'Pedal'],
        explanation: 'They are all types of bin',
      },
    ],
  },

  lunchAtLunch: {
    code: 'USE A TRAY',
    wall: [
      {
        regex: /lunch/i,
        explanation: 'They are all common lunchtime activities',
        items: ['Croquet', 'Salem', 'Client Call', 'Kings Arms'],
      },
      {
        regex: /julie|jules/i,
        explanation: 'They are all jobs that Jules does',
        items: ['Kitchen', 'Ironing', 'Dog Walking', 'Cleaning'],
      },
      {
        regex: /room/i,
        explanation:
          'They are all names by which rooms at Heath Hall are known',
        items: ['Tony Hodges', 'Library', 'CST', 'Right Foot'],
      },
      {
        regex: /board/i,
        explanation: 'They are all board members',
        items: [
          'Vivienne Hodges',
          'Francis Goss',
          'Peter Nicholas',
          'Trevor Posliff',
        ],
      },
    ],
  },
  peopleAtLunch: {
    code: 'TAKE IT SERIOUSLY',
    wall: [
      {
        regex: /james/i,
        explanation: 'They are all Jameses',
        items: ['T Wilson', 'Hiley', 'Bond', 'Martin'],
      },
      {
        regex: /castaway|cast away/i,
        explanation: 'They are associated with the film Castaway',
        items: ['Beach', 'Wilson', 'Hanks', 'Chuck'],
      },
      {
        regex: /tom/i,
        explanation: 'They are all Toms',
        items: ['Illes', 'Pearson', 'Lawton', 'Cruise'],
      },
      {
        regex: /letters/i,
        explanation:
          'They are have other names as homophones for letters. Vernon Kay, Bee Rycroft, Cee Lo Green, Elle Macpherson',
        items: ['Vernon', 'Green', 'Rycroft', 'MacPherson'],
      },
    ],
  },

  familyQuiz: {
    code: 'THE QUIZ WILL RETURN',
    wall: [
      {
        regex: /rice/i,
        items: ['White', 'Brown', 'Lemon', 'Sticky'],
        explanation: 'They are all types of rice',
      },
      {
        regex: /tea/i,
        items: ['Jasmine', 'Green', 'Cream', 'High'],
        explanation: 'They are all types of tea',
      },
      {
        regex: /black/i,
        items: ['Flag', 'Berry', 'Bag', 'Box'],
        explanation: "They can all be preceded by 'black'",
      },
      {
        regex: /bin|bins/i,
        items: ['Salt', 'Bread', 'Dust', 'Pedal'],
        explanation: 'They are all types of bin',
      },
    ],
  },
};

export default walls;
