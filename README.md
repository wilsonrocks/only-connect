# Shelley Connect

## What is it?

An interactive clone of the BBC quiz Only Connect's Connecting Wall round, built in React with Framer Motion

## Try it

[https://shelley-connect.netlify.app/familyQuiz](https://shelley-connect.netlify.app/familyQuiz)

```bash
yarn
yarn run start # dev server on port 3000
```

You can add new walls in `walls.js` and they are selected via the URL.

## What did I learn?

- The logic behind the display of the connecting wall is _hard_.
- Framer Motion is much easier to use than React Spring, if you must use a JS animation solution.
- It's harder than you think to design a connecting wall, and people can solve it much faster than you can make it.
